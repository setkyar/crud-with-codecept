@extends('layout.main')

@section('content')
    
    <h1>View Posts</h1>
	
	@if (Session::has('message'))
    	<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif

	<table class="table table-striped table-bordered">
		<thead>
			<td>Post Title</td>
			<td>View Post</td>
			<td>Edit Post</td>			
			<td>Delete Post</td>
		</thead>    			
		<tbody>
			@foreach($posts as $post)
				<tr>
					<td>{{ $post->title }}</td>
					<td>
						<a class="btn btn-small btn-success" href="{{ URL::to('posts/' . $post->id) }}">View this Post</a>
					</td>
					<td>
						<a class="btn btn-small btn-info" href="{{ URL::to('posts/' . $post->id . '/edit') }}">Edit Post</a>
					</td>
					<td>
						{{ Form::open(array('url' => 'posts/' . $post->id)) }}
		                    {{ Form::hidden('_method', 'DELETE') }}
		                    {{ Form::submit('Delete this Post', array('class' => 'btn btn-warning')) }}
		                {{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
    
@stop