@extends('layout.main')

@section('content')
    
    <h1>Create Posts</h1>
	
	{{ Form::open(array('url' => 'posts')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('body', 'Body') }}
        {{ Form::textarea('body', Input::old('body'), array('class' => 'form-control')) }}
    </div>

    	{{ Form::submit('Create an Artices!', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}

@stop