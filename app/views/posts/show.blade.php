@extends('layout.main')

@section('content')
    
    <h1>Showing - {{ $post->title }}</h1>
	
	@if (Session::has('message'))
    	<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif

	<div class="jumbotron">
		<h2>{{ $post->title }}</h2>
		
		<p class="lead">{{ $post->body }}</p>
	</div>
@stop

