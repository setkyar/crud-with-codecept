<?php 

$I = new AcceptanceTester($scenario);
$I->wantTo('verify that the home page redirect to post with message');

$I->amOnPage('/');
$I->seeCurrentUrlEquals('/posts');
$I->see('Sorry, I just write this for basically with bdd!, Hope you understand.');