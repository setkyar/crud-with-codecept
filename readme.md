## CRUD with Codeception

### Database Setting

Change your database credentials on 

	app/config/database.php

&&

	codeception.yml

### Usage

Run Server with 

	$ php artisan serve

See it on localhost:8000

Run Test with 
	
	$ vendor/bin/codecept run




