<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
	Session::flash('message', 'Sorry, I just write this for basically with bdd!, Hope you understand.');
	return Redirect::route('posts.index');
});

Route::resource('posts', 'PostsController');