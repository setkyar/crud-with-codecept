@extends('layout.main')

@section('content')
    
    <h1>Edit {{ $post->title }}</h1>
	
	{{ Form::model($post, array('route' => array('posts.update', $post->id), 'method' => 'PUT')) }}

	    <div class="form-group">
	        {{ Form::label('title', 'Title') }}
	        {{ Form::text('title', null, array('class' => 'form-control')) }}
	    </div>

	    <div class="form-group">
	        {{ Form::label('body', 'Body') }}
	        {{ Form::textarea('body', null, array('class' => 'form-control')) }}
	    </div>

	    {{ Form::submit('Edit Post!', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}
    
@stop