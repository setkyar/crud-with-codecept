<html>
	<head>
		<title>Laravel CRUD with BDD</title>
		<link rel="stylesheet" href="/assert/css/bootstrap.css">
	</head>
	
	<body>
		<div class="container">
			<nav class="navbar navbar-inverse">
    			<div class="navbar-header">
        			<a class="navbar-brand" href="{{ URL::to('posts') }}">Posts Collect!</a>
    			</div>

			    <ul class="nav navbar-nav">
			        <li><a href="{{ URL::to('posts') }}">View All Posts</a></li>
			        <li><a href="{{ URL::to('posts/create') }}">Create a article</a>
			    </ul>
			</nav>
			
			@yield('content')
		</div>
		<script src="/assert/js/jquery-1.10.2.min.js"></script>
		<script src="/assert/js/bootstrap.js"></script>
	</body>
</html>