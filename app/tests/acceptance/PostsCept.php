<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('verify that Posts Resource is working');

$I->amOnPage('/posts');
$I->see('View Posts', 'h1');

$I->click('Create a article');
$I->seeCurrentUrlEquals('/posts/create');

$I->click('View All Posts');
$I->seeCurrentUrlEquals('/posts');

//verify that Posts Create is working
$I->amOnPage('/posts/create');
$I->fillField('title', 'Hello World');
$I->fillField('body', 'This is body');
$I->click('Create an Artices!');
$I->seeCurrentUrlEquals('/posts');
$I->see('Successfully created Artice!');

//verify that post view is working
$I->amOnPage('/posts');
$posts_title = $I->grabTextFrom('table>tbody>tr>td');
$I->click('View this Post');
$I->see('Showing - '. $posts_title, 'h1');
$I->see($posts_title, 'h2');
$post_body = $I->grabTextFrom('.lead');

//verify that post edit is working
$I->amOnPage('/posts');
$I->click('Edit Post');
$I->see('Edit ' . $posts_title);
$I->click('Edit Post!');
$I->seeCurrentUrlEquals('/posts');
$I->see('Successfully updated your post!');

//verify that post delete is working
$I->amOnPage('/posts');
$I->click('Delete this Post');
$I->see('Successfully deleted the post!');
